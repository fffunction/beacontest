//
//  ViewController.swift
//  beaconTest
//
//  Created by Dan Reeves on 20/01/2015.
//  Copyright (c) 2015 Dan Reeves. All rights reserved.
//

import UIKit;

class ViewController: UIViewController, KTKLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var button: UIBarButtonItem!
    @IBOutlet weak var textField: UILabel!;
    @IBOutlet weak var tableView: UITableView!;
    
    var logging: Bool = false;
    var fileName: String = "default";
    let dirs: [String]? = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String]
    var logs: String = "";
    var beaconList: [CLBeacon] = [];
    let locationManager : KTKLocationManager = KTKLocationManager();
    let lengthFormatter = NSLengthFormatter();
    
    override func viewDidLoad() {
        super.viewDidLoad();

        button.action = "startLogging";
        
        if (KTKLocationManager.canMonitorBeacons())
        {
            var uuids = [
                "f7826da6-4fa2-4e98-8024-bc5b71e0893e",
                "3FD30054-3F7F-42F8-A3B1-322E88FD936B",
                "2650E58E-7395-4687-933E-8F2E6B775224",
                "DB89582B-A6F4-4C4D-8D18-81D0A455456F",
                "1B128855-88D2-4C1A-8E97-D86300D96C99",
                "BD83A8A4-0AFA-4692-B668-B3A3D2435470",
                "D091931E-5C78-41C5-B5DD-52D21CFBB4C2",
                "D78C1BBA-49F9-4ECC-83B8-5BEB902B8683",
                "F1569C88-C122-481E-8DDD-6B87B50406FD",
                "D0711EA1-6F06-4E78-819D-A8746EF4B79F",
                "07A006B7-CF58-4AA1-97C2-81625CC463E9"
            ];

            var regions : [KTKRegion] = [];
            
            for uuid in uuids {
                var r : KTKRegion = KTKRegion();
                r.uuid = uuid;
                regions.append(r);
            }
            
            self.locationManager.setRegions(regions);
            self.locationManager.delegate = self;
            self.locationManager.startMonitoringBeacons();
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        return false;
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientation.Portrait.rawValue;
    }
    
    override func viewDidAppear(animated: Bool) {
        println("Listening...");
//        self.locationManager.startMonitoringBeacons();
    }

    override func viewDidDisappear(animated: Bool) {
        println("Not listening.");
//        self.locationManager.stopMonitoringBeacons();
    }


    func locationManager(locationManager: KTKLocationManager!, didChangeState state: KTKLocationManagerState, withError error: NSError!) {
        if (state == .Failed)
        {
            println("Something went wrong with your Location Services settings. Check your settings.");
        }
    }

    func locationManager(locationManager: KTKLocationManager!, didEnterRegion region: KTKRegion!) {
        println("Enter region \(region.identifier)");
        
        let date = NSDate()
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        var d = formatter.stringFromDate(date)
        
        
        var localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertAction = "Enter:\(region.identifier) at \(d)"
        localNotification.alertBody = "Enter:\(region.identifier) at \(d)"
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 0)
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }

    func locationManager(locationManager: KTKLocationManager!, didExitRegion region: KTKRegion!) {
        println("Exit region \(region.identifier)");
        
        let date = NSDate()
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        var d = formatter.stringFromDate(date)
        
        var localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertAction = "Exit:\(region.identifier) at \(d)"
        localNotification.alertBody = "Exit:\(region.identifier) at \(d)"
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 0)
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }

    func locationManager(locationManager: KTKLocationManager!, didRangeBeacons beacons: [AnyObject]!) {
        beaconList = [];
        var filteredBeacons = beacons.filter({$0.accuracy > 0});
        if filteredBeacons.count < 1 {
            textField.text = "Searching...";
            tableView.reloadData();
            return
        }
        var closest: CLBeacon = filteredBeacons.first as CLBeacon;
        for beacon in filteredBeacons {
            var b: CLBeacon = beacon as CLBeacon;
            if self.logging {
                self.logs += "\(Timestamp),\(b.minor),\(b.accuracy),\(b.rssi)\n"
            }
            beaconList.append(b);
            if b.rssi > closest.rssi {
                closest = b;
            }
        }
        var length = lengthFormatter.stringFromMeters(round(closest.accuracy * 100) / 100);
        textField.text = "Nearest is beacon \(closest.minor) at \(length)";
        tableView.reloadData();
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "There are \(self.beaconList.count) beacons in range"
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beaconList.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: nil);
        cell.selectionStyle = UITableViewCellSelectionStyle.None;
        cell.textLabel?.text = "ID: \(self.beaconList[indexPath.row].minor)";
        
        var length = lengthFormatter.stringFromMeters(round(self.beaconList[indexPath.row].accuracy * 100)/100);
        
        var proximity : String = "Unknown";
        
        switch self.beaconList[indexPath.row].proximity {
            case CLProximity.Immediate:
                proximity = "Immediate";
            case CLProximity.Near:
                proximity = "Near";
            case CLProximity.Far:
                proximity = "Far";
            default:
                proximity = "Unknown";
        }
        
        cell.detailTextLabel?.text = "\(proximity): \(length)";
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //CODE TO BE RUN ON CELL TOUCH
        println("ID: \(self.beaconList[indexPath.row].minor)")
        if let url = NSURL(string: "http://beacons.fffixture.co/beacon/\(self.beaconList[indexPath.row].minor)") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func startLogging () {
        //1. Create the alert controller.
        var alert = UIAlertController(title: "New Log", message: "Name your file", preferredStyle: .Alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.placeholder = ""
        })
        
        //3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            println("Text field: \(textField.text)")
            
            self.fileName = textField.text;
            self.button.title = "Stop logging";
            self.button.action = "stopLogging";
            self.logging = true;
        }))
        
        // 4. Present the alert.
        self.presentViewController(alert, animated: true, completion: nil)

    }
    
    func stopLogging () {
        self.logging = false;
        button.action = "";
        
        let directories:[String] = dirs!
        let dir = directories[0]; //documents directory
        let path = dir.stringByAppendingPathComponent(self.fileName) + ".csv";

        //writing
        self.logs.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding, error: nil);
        self.logs = "";
        
        button.action = "startLogging";
        self.fileName = "default";
        button.title = "Start logging";

    }

    
}
var Timestamp: NSTimeInterval {
    get {
        return round(NSDate().timeIntervalSince1970)
    }
}